package Controller;

import Model.MusicGroup;
import Model.Song;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.swing.JOptionPane;

public class PlayerM {
	public static Player player;
	private BufferedInputStream BIS;
	AudioInputStream as;
	
	public long pauseLocation;
	public long totalSongLength;
	public FileInputStream FIS;
	public String PathResume;
	private static String route="src/music/";
	
	
	View.PlayerMusic PM;
	Integer idP;
	Integer idUser;
	
	public PlayerM(int id,Controller.MainView MV,Integer idP, Integer idU) {
		this.idP = idP;
		this.idUser= idU;
		PM = new View.PlayerMusic();
		//Button Add Song in playlist
		if(idP==null) {
		PM.lblAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new PlayListC(id);
			}
		});
		}
		
		//Button Pause
		PM.lblPause.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Pause();
			}
		});
		
		
		//Button Stop
		PM.lblStop.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(player!=null) player.close();
				pauseLocation = 0;
				totalSongLength= 0;
				PathResume = null;
			}
		});
		
		
		//Button Play
		PM.lblPlay.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(PathResume==null) Play(route+"Thousand Foot Krutch - Incomplete.mp3");
				else Resume();
				
			}
		});
		
		//button Undo
		PM.lblUndo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(idP!=null) MV.FindbyPlaylist(idP);
				else if(idU!=null) {MV.UserProfile(idU);}
				else MV.PopularNow();
				if(player!=null) player.close();
				pauseLocation = 0;
				totalSongLength= 0;
				PathResume = null;
			}
		});
		
		
		Song s = Song.load(id);
		MusicGroup mg = MusicGroup.load(s.id_group);
		PM.lblTextMusic.setText(mg.name+": "+s.name);

	}
	
	
	//Play Control
	private void Play(String path) {
		
		//Loading the Song...
		try {
			FIS = new FileInputStream(path);
			BIS = new BufferedInputStream(FIS);
			
			
			
			totalSongLength = FIS.available();
			PathResume = path;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try {
			
			if(player!=null) player.close();
			
			player=new Player(BIS);
			
			 new Thread() {
				    
				      public void run(){
				        try{
				          player.play();
				        }catch(Exception e){
				          JOptionPane.showMessageDialog(null, "Error playing mp3 file");
				        }
				       }}.start();
  
		} catch (JavaLayerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		     
	}
	
	//Pause Control
		private void Pause() {
			if(player!=null) {
				try {
					pauseLocation = FIS.available();
					player.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		//Resume Control
		private void Resume() {
			
			//Loading the Song...
			try {
				if(player!=null) player.close();
				FIS = new FileInputStream(PathResume);
				BIS = new BufferedInputStream(FIS);
				
				player=new Player(BIS);
			//Skip to Resume part
				FIS.skip(totalSongLength-pauseLocation);
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
						
				
				new Thread() {
					    
					      public void run(){
					        try{
					          player.play();
					        }catch(Exception e){
					          JOptionPane.showMessageDialog(null, "Error playing mp3 file");
					        }
					       }}.start();
	  		     
		}
	
	
	
}
