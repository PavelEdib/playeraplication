package Controller;

import Model.User_Playlist;
import Other.Datos;
import View.PlayList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PlayListC {
		public int idSong;
		View.PlayList PL;
	public PlayListC(int idS) {
		this.idSong = idS;
		PL = new PlayList();
		//Add Song In PlayList
		PL.btnAddSong.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ADDSongPlaylist();
				PL.dispose();
			}
		});
		//Create new Playlist
		PL.btnCreatePlaylist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				newPlaylist();
				FillComboPlaylist();
			}
		});
		
		FillComboPlaylist();
		
		
	}
	
	private void FillComboPlaylist() {
		PL.comboBoxPlayList.removeAll();
		for(User_Playlist up:User_Playlist.find(MainView.user.id_user)) {
			PL.comboBoxPlayList.addItem(up);
		}
		PL.repaint();
		PL.revalidate();
	}
	
	private void newPlaylist() {
		if(PL.tfNewPlayList.getText().isBlank()) {
			Datos.mError("Fill the Text Box");
		}else {
			User_Playlist.Create(PL.tfNewPlayList.getText(), 1,MainView.user.id_user);
			Datos.mInfo("Created Succeful");;
		}
		PL.tfNewPlayList.setText("");
	}
	
	private void ADDSongPlaylist() {
		User_Playlist up = (User_Playlist)PL.comboBoxPlayList.getSelectedItem();
		up.addSong(this.idSong);
		
	}
	
	
	
}
