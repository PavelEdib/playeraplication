package Controller;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import Model.User;
import Other.Crypt;
import Other.Datos;

import java.awt.event.ActionEvent;



public class Login {
	public View.Login lg;
	private User utemp; 
		
		public Login() {
			lg  = new View.Login();
			//click on Buton Login
			lg.btnLogin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					if(CheckDates()) {
						lg.dispose();
						new MainView(utemp);
					}else {
						Datos.mError("Invalid Password");
					}
				}
			});
			
			//Click Button Register
			lg.btnLogUp.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new Controller.Register();
				}
			});
			lg.setVisible(true);
			
		}
		
		//Method to check Dates of User
		private boolean CheckDates() {
			boolean op = true;
			utemp = null;
			
			LinkedList<User> TempList = new LinkedList<User>();
			TempList = User.find(lg.tfUsername.getText(), null);

			for(User u:TempList) {
				if(u.username.equalsIgnoreCase(lg.tfUsername.getText())){
					utemp = u;
				}
			}
			
			if(utemp ==null) {
				op=false;
				Datos.mError("Invalid Username!");
			}else {
				if(!utemp.password.equals(Crypt.Crypting(new String(lg.passField.getPassword())))) {
					op = false;
				}
			}
			
			return op;
		}

		

		
}
