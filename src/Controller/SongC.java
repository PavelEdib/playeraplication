package Controller;



import Model.Gen;
import Model.MusicGroup;
import Model.Song;
import Other.Datos;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class SongC {
	View.Song SW;
	
	public SongC() {
		SW = new View.Song();
		
		//btn Create new Song
		SW.btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(VerifyDates()) {
					InsertSong();
					MusicControl.LoadSongs();
					SW.dispose();
				}else {
					Datos.mError("Fill Fields Song's Name and Duration");
				}
			}
		});
		
		
		
		
		
		//Add New Type
		SW.btnAddType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InsertType();
			}
		});
		
		//Add New Music Group
		SW.btnAddGroup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InsertDateGroup();
			}
		});
		
		//Disable BTN Delete
		SW.btnDelete.setEnabled(false);
		
		//Fill the Combobox
		FillGroupM();
		FillGen();
	
	}
	
	
	public SongC(int id) {
		SW = new View.Song();
		
		//btn Create new Song
		SW.btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(VerifyDates()) {
					Update(id);
					MusicControl.LoadSongs();
					SW.dispose();
				}else {
					Datos.mError("Fill Fields Song's Name and Duration");
				}
			}
		});
		
		SW.btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Model.Song.Delete(id);
				MusicControl.LoadSongs();
				SW.dispose();
			}
		});
		
		
		//Add New Type
		SW.btnAddType.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InsertType();
			}
		});
		
		//Add New Music Group
		SW.btnAddGroup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				InsertDateGroup();
			}
		});
		
		
		//Fill the Combobox
		FillGroupM();
		FillGen();
		//Fill Form
		FillForm(id);

	}
	
	
	
	
	//----Methods for Insert a Song
		private boolean VerifyDates() {
			boolean res = true;

			if(SW.tfName.getText().isEmpty()) res = false;
			if(SW.tfDuration.getText().isEmpty()) res = false;
			
			return res;
		}
		
		private void Update(int id) {
			String name = SW.tfName.getText();
			MusicGroup Mgroup =(MusicGroup)SW.comboBoxGroup.getSelectedItem();
			Gen g = (Gen)SW.comboBoxGen.getSelectedItem();
			String Dur = SW.tfDuration.getText();
			
			Song s = Song.load(id);
			
			s.name = name;
			s.duration = Dur;
			s.id_group = Mgroup.id_group;
			s.id_gen = g.id_gen;
			
			s.Update();
			
			Datos.mInfo("Succefull Updated!!");
		}
	
		private void InsertSong() {
			String name = SW.tfName.getText();
			MusicGroup Mgroup =(MusicGroup)SW.comboBoxGroup.getSelectedItem();
			Gen g = (Gen)SW.comboBoxGen.getSelectedItem();
			String Duration = SW.tfDuration.getText();
			
			Song s =  Song.Create(name, Mgroup.id_group, g.id_gen, Duration);
			Datos.mInfo("Song Id: "+s.id_song);
		}
	//----Fin Methods for Insert a Song
		
	
	
	//--Fill The Form
		private void FillForm(int id) {
			Song s = Song.load(id);
			
			SW.tfName.setText(s.name);
			SW.tfDuration.setText(s.duration);
			
			
			for(int i = 0; i<SW.comboBoxGroup.getItemCount();i++) {
				
				if(s.id_group == ((MusicGroup)(SW.comboBoxGroup.getItemAt(i))).id_group) {
					SW.comboBoxGroup.setSelectedIndex(i);
					break;
				}
				
			}
			
			for(int i = 0; i<SW.comboBoxGen.getItemCount();i++) {
				
				if(s.id_gen == ((Gen)(SW.comboBoxGen.getItemAt(i))).id_gen) {
					SW.comboBoxGen.setSelectedIndex(i);
					break;
				}
			}
				
			
			
			
		}
		
	//Insert a Group
	private void InsertDateGroup() {
		if(!SW.tfAddGroup.getText().isEmpty()) {
			MusicGroup.Create(SW.tfAddGroup.getText());
			Datos.mInfo("Succeful!!");
		}else {
			Datos.mError("The TextField is Empty!");
		}
		FillGroupM();
	}
	
	//Insert a Type
	private void InsertType() {
		if(!SW.tfAddType.getText().isEmpty()) {
			Gen.Create(SW.tfAddType.getText());
			Datos.mInfo("Succeful!!");
		}else {
			Datos.mError("The TextField is Empty!");
		}
		FillGen();
	}
	
	
	
	private void FillGroupM() {
		
		SW.comboBoxGroup.removeAllItems();
		for(MusicGroup mg:MusicGroup.find(null)) {
			SW.comboBoxGroup.addItem(mg);
		}
		SW.revalidate();
		SW.repaint();
	}
	
	private void FillGen() {
		
		SW.comboBoxGen.removeAllItems();
		for(Gen g:Gen.All()) {
			SW.comboBoxGen.addItem(g);
		}
		SW.revalidate();
		SW.repaint();
	}
	
}
