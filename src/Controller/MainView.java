package Controller;


import Model.MusicGroup;
import Model.Song;
import Model.User;
import Model.User_Playlist;
import Other.PlaylistLabel;
import Other.SongLabel;
import Other.UserLabel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;

import java.awt.event.ActionListener;

import java.awt.event.ActionEvent;

public class MainView {
	public static User user;
	View.MainView MV;
	Controller.PlayerM PM;
	
	public MainView(User user) {
		MainView.user = user;
		MV = new View.MainView();
		
		//Label Button Friends
		MV.lblFriends.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Friends();
			}
		});
		//Label Button Playlist
		MV.lblPlaylists.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(PlayerM.player!=null) PlayerM.player.close();
				Playlist();
			}
		});
		
		//label Popular Now
		MV.lblMusic.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(PlayerM.player!=null) PlayerM.player.close();
				PopularNow();
			}
		});
		
		//btn Find
		MV.btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!MV.tfSearch.getText().isBlank()) {
					MV.Panel_PopularNow.removeAll();
					Find(MV.tfSearch.getText());
					MV.tfSearch.setText("");
					HeaderPanel();
					MV.repaint();
					MV.revalidate();
				}
			}
		});
		
		//btn Find Users
		MV.btnFindUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!MV.tfFinduser.getText().isBlank()) {
					MV.Panel_PopularNow.removeAll();
					HeaderPanelUser();
					FindUser(MV.tfFinduser.getText());
					MV.tfFinduser.setText("");
					
					MV.repaint();
					MV.revalidate();
						}
					}
		});
		
		if(user.getPriv() !=3) MV.mnAdmin.setVisible(false);
		//Admin Cotrol for Users 
		MV.mntmGroup.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Controller.UsersControl();
			}
		});
		//Admin control for Songs 
		MV.mntmSongs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Controller.MusicControl();
			}
		});
		
	
		
		//Log Out
		MV.lblLogOut.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new Controller.Login();
				MV.dispose();
			}
		});
		
		
		
	
	}
	
	//get A list of Songs in labels
	private void Find(String Name) {
		int x = 30;
		int y = 130;
		LinkedList<SongLabel> listLabel = new LinkedList<>(); 	
			
			for(Song s:Song.find(Name)) {
				
				MusicGroup mg = MusicGroup.load(s.id_group);
				SongLabel sl = new SongLabel(mg.name +" - "+s.name,x,y,s.id_song,null,null);
				MV.Panel_PopularNow.add(sl);
				Action(sl);
				listLabel.add(sl);
				y+=70;		
			}

			MV.repaint();
			MV.revalidate();
	}
	
	//Action for Every Label Song
	private void Action(SongLabel lbl) {
		Controller.MainView main =this;
	lbl.addMouseListener(new MouseAdapter() {
		
		@Override
		public void mouseClicked(MouseEvent e) {
			PM = new Controller.PlayerM(lbl.id,main,lbl.idPlaylist,lbl.idUser);
			
			MV.getContentPane().remove(MV.scrollPaneMain);
			MV.getContentPane().add(PM.PM);
			
			MV.repaint();
			MV.revalidate();
		}
		});
	
	}
	
	//header the Look for songs
	public void HeaderPanel() {
		MV.Panel_PopularNow.add(MV.panelHeader);
		
		MV.repaint();
		MV.revalidate();
	}
	
	//Lbl button Popular Now
	public void PopularNow() {
		
		if(PM!=null) {
			MV.getContentPane().removeAll();
			MV.getContentPane().add(MV.scrollPane);
			MV.getContentPane().add(MV.scrollPaneMain);
		}
		
		MV.Panel_PopularNow.removeAll();
		HeaderPanel();
		Find(null);
		MV.repaint();
		MV.revalidate();
		
	}
	
	
	//------Methods for Playlist--------
	
	//lbl button Playlist
	public void Playlist() {
		
		if(PM!=null) {
			MV.getContentPane().removeAll();
			MV.getContentPane().add(MV.scrollPane);
			MV.getContentPane().add(MV.scrollPaneMain);
		}
		
		MV.Panel_PopularNow.removeAll();
		MV.lblPlay.setText("Playlists");
		MV.Panel_PopularNow.add(MV.lblPlay);
		FindP();
		MV.repaint();
		MV.revalidate();
		
	}
	
	//Find Playlist
	public void FindP() {
		int x = 30;
		int y = 130;
		LinkedList<PlaylistLabel> listLabel = new LinkedList<>(); 	
			
			for(User_Playlist UP:User_Playlist.find(user.id_user)) {
				
				PlaylistLabel pl = new PlaylistLabel(UP.name, x, y, UP.id_Playlist);
				MV.Panel_PopularNow.add(pl);
				ActionPlLabel(pl);
				listLabel.add(pl);
				y+=100;		
			}

			MV.repaint();
			MV.revalidate();
	}
	
	//Action for Label of Playlist
	private void ActionPlLabel(PlaylistLabel pl) {
		pl.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
				FindbyPlaylist(pl.id);
				MV.repaint();
				MV.revalidate();
			}
			});
		
	}
	
	//find songs by playlist's id
	public void FindbyPlaylist(int id) {
		int x = 30;
		int y = 130;
		
		if(PM!=null) {
			MV.getContentPane().removeAll();
			MV.getContentPane().add(MV.scrollPane);
			MV.getContentPane().add(MV.scrollPaneMain);
		}
		
		MV.Panel_PopularNow.removeAll();
		MV.lblPlay.setText("Songs");
		MV.Panel_PopularNow.add(MV.lblPlay);
		
		User_Playlist usp = User_Playlist.load(id);
		LinkedList<SongLabel> listLabel = new LinkedList<>(); 	
			
			for(Song s:usp.GetSongs()) {
				
				MusicGroup mg = MusicGroup.load(s.id_group);
				SongLabel sl = new SongLabel(mg.name +" - "+s.name,x,y,s.id_song,usp.id_Playlist,null);
				MV.Panel_PopularNow.add(sl);
				Action(sl);
				listLabel.add(sl);
				y+=70;		
			}

			MV.repaint();
			MV.revalidate();
	}
	
	//--------Fin Playlist---------
	
	//-------- Init Label btn Friends-----
	public void Friends() {
		MV.Panel_PopularNow.removeAll();
		HeaderPanelUser();
		FindFriends();
		MV.repaint();
		MV.revalidate();
	}
	
	public void HeaderPanelUser() {
		MV.Panel_PopularNow.removeAll();
		MV.Panel_PopularNow.add(MV.panelHeaderUser);
	}
	
	public void FindFriends() {
		int x = 30;
		int y = 130;
		LinkedList<UserLabel> listUser = new LinkedList<>(); 	
			
			for(User u:user.getFriend()) {
				
				UserLabel ul = new UserLabel(u.first_name +" "+u.last_name, x, y,u.id_user);
				MV.Panel_PopularNow.add(ul);
				ActionULabel(ul);
				listUser.add(ul);
				y+=100;		
			}

			MV.repaint();
			MV.revalidate();
	}
	
	
	public void FindUser(String username) {
		int x = 30;
		int y = 130;
		LinkedList<UserLabel> listUser = new LinkedList<>(); 
		
		for(User u :User.find(username, null)) {
			UserLabel ul = new UserLabel(u.first_name +" "+u.last_name, x, y,u.id_user);
			ActionULabel(ul);
			MV.Panel_PopularNow.add(ul);
			
			listUser.add(ul);
			y+=100;	
		}
		
		MV.repaint();
		MV.revalidate();
		
	}
	
	public void ActionULabel(UserLabel ul) {
		ul.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				UserProfile(ul.idUser);
			}
			});
		
	}
	
	private void PanelUser(int idU) {
		User uLocal = User.load(idU);
		MV.lblNameUser.setText(uLocal.first_name+" "+uLocal.last_name);
		
		if(user.isFriend(idU)) 	MV.btnFollow.setText("Unfollow");
		else 	MV.btnFollow.setText("Follow");	
			
		//btnfollow with action to ADD or Remove a friend
		MV.btnFollow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(user.isFriend(idU)) {
					user.RemoveFriend(idU);
					MV.btnFollow.setText("Follow");
				}else {	
					user.ADDFriend(idU);
					MV.btnFollow.setText("Unfollow");
				}
				MV.repaint();
				MV.revalidate();
			}
		});
		
		
		
		
		MV.Panel_PopularNow.add(MV.panelUserProfile);
	}
	
	public void UserProfile(int idU) {
		
		if(PM!=null) {
			MV.getContentPane().removeAll();
			MV.getContentPane().add(MV.scrollPane);
			MV.getContentPane().add(MV.scrollPaneMain);
		}
		
		MV.Panel_PopularNow.removeAll();
		PanelUser(idU);
		
		int x = 30;
		int y = 200;
		LinkedList<SongLabel> listLabel = new LinkedList<>(); 	
			
			for(Song s:Song.getAllSongs(idU)) {
				
				MusicGroup mg = MusicGroup.load(s.id_group);
				SongLabel sl = new SongLabel(mg.name +" - "+s.name,x,y,s.id_song,null,idU);
				MV.Panel_PopularNow.add(sl);
				Action(sl);
				listLabel.add(sl);
				y+=70;	
			}

			MV.repaint();
			MV.revalidate();
	}
	
}
