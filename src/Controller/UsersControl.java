package Controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;



import Model.User;
import Other.Fecha;
import Other.Tabla;
import View.AdminControlView;

public class UsersControl {
		AdminControlView adU;
		private Tabla UserTable;
		Integer id;
		View.User us;
		
		public UsersControl() {
			adU = new AdminControlView();
			adU.remove(adU.panelHeader);
			adU.setBounds(100, 100, 1100, 676);
			adU.scrollPane.setBounds(0, 0, 1086, 608);
			
			TableUsers();
			
			//Double Click on Register from Table 
			UserTable.tabla.addMouseListener(new MouseAdapter() {
				
				@Override
				public void mouseClicked(MouseEvent e) {
					
					if (e.getClickCount()==2 && e.getButton()==1) {
						id=(int)UserTable.getValueSelected(0);
						us = new View.User();
						FillUser(id);
						
						
						//form User btn Delete
						us.btnDelete.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								User.delete(id);
								TableUsers();
								us.dispose();
							}
						});
						
						//form User btn Save
						us.btnSave.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								Model.User u = Model.User.load(id);
								if(!u.password.equals(us.tfPass.getText())) {
									u.password = Other.Crypt.Crypting(us.tfPass.getText());
									u.update();
								}
								u.setPriv((int)us.spinnerPriv.getValue());
								TableUsers();
								us.dispose();
							}
						});
					}
				}	
			});
			
			
			
		
			
			
			
		}
		
		//create Table for Users 
		private void TableUsers() {
			Object[] header = {"ID","First Name","Last Name","mail","Phone Number","Birth Date","username","Pass","Priv"};
			Class<?>[] type = {Integer.class,String.class,String.class,String.class,String.class,Fecha.class,String.class,String.class,Integer.class};
			Integer[] size = {25,120,120,150,100,80,120,160,30};
			
			UserTable = new Tabla(header,type,size,null,null,null);
			adU.scrollPane.setViewportView(UserTable.tabla);
			LoadUsers();
			adU.revalidate();
			adU.repaint();
			
		}
		
		private void LoadUsers() {
			LinkedList<User> UserList = new LinkedList<>();
			
			UserList = User.find(null, null);
			
			for(User u:UserList) {
				Object[] fila = {u.id_user,u.first_name,u.last_name,u.mail,u.PhoneN,u.BirthDate,u.username,u.password,u.getPriv()};
				UserTable.modelo.addRow(fila);
			}
		}
		
		private void FillUser(int id) {
			Model.User u = Model.User.load(id);
			
			us.lblUser.setText(u.first_name+" "+u.last_name);
			us.tfUsername.setText(u.username);
			us.tfPass.setText(u.password);
			us.spinnerPriv.setValue(u.getPriv());
		}
		
		
}
