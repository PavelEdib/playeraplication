package Controller;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import Model.User;
import Other.Crypt;

import java.awt.event.ActionEvent;

public class Register {
		View.Register rg;
		
		
	public Register() {
		rg = new View.Register();
		rg.btnSignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(CheckDates()) {
					Other.Fecha BD = new Other.Fecha(rg.dateChooser.getDate());
					
					Model.User u= Model.User.Create(rg.tfFname.getText(),
													rg.tfLname.getText(), 
													rg.tfMail.getText(), 
													rg.tfPhone.getText(), 
													BD, 
													rg.tfUsername.getText(), 
													Crypt.Crypting(new String(rg.tfPass.getPassword())));
					
					if(u!=null) Other.Datos.mInfo("User Registered Succeful!");
						rg.dispose();
				}else {
					Other.Datos.mError("Fill all Fields");
				}
			}
		});

	}
	
	
	private boolean CheckDates() {
		boolean temp = true;
		
		if(rg.tfFname.getText().isEmpty()) temp = false;
		if(rg.tfLname.getText().isEmpty()) temp = false;
		if(rg.tfMail.getText().isEmpty()) temp = false;
		if(rg.tfPhone.getText().isEmpty()) temp = false;
		if(rg.tfUsername.getText().isEmpty()) temp = false;
		else {
			//Check username doublicate
			LinkedList<User> TempList = new LinkedList<User>();
			TempList = User.find(rg.tfUsername.getText(), null);

			for(User u:TempList) {
				if(u.username.equalsIgnoreCase(rg.tfUsername.getText())){
					temp = false;
					Other.Datos.mError("This username is used");
				}
			}
			
		}
		
		if(rg.tfPass.getPassword().length ==0) {
			temp = false;
		}else {
			if(!new String(rg.tfPass.getPassword()).equals(new String(rg.tfPassRepeate.getPassword()))) {
				Other.Datos.mError("Invalid Password in second Field");
				temp=false;
			}
		}
		if(rg.dateChooser.getDate()==null)  temp=false;
		
		
		return temp;
	}
	
}
