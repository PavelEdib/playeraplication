package Controller;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;

import Model.Gen;
import Model.MusicGroup;
import Model.Song;
import Other.Tabla;
import View.AdminControlView;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MusicControl {
	static AdminControlView adM;
	private static Tabla musicTable;
	
	public MusicControl() {
		adM = new AdminControlView();
		adM.btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Find();
				adM.tfFind.setText("");
			}
		});
		
		TableSongs();
		
		//Double Click on Register from Table 
		musicTable.tabla.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount()==2 && e.getButton()==1) {
					Integer id=(int)musicTable.getValueSelected(0);
					new SongC(id);
				}
			}
		});
		
		//Insert new Song
		adM.lblNewSong.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new SongC();
			}
		});
		
	}
	//create Table for Songs
	private void TableSongs() {
		Object[] header = {"ID","Name","Group","Gen","Duration"};
		Class<?>[] type = {Integer.class,String.class,String.class,String.class,String.class};
		Integer[] size = {50,150,150,100,50};
		
		musicTable = new Tabla(header,type,size,null,null,null);
		Object[] fila= {1,"Music","zdob"," rock","1:34"};
		musicTable.modelo.addRow(fila);
		LoadSongs();
		adM.scrollPane.setViewportView(musicTable.tabla);
		
		
		
	}
	
	
	public static void LoadSongs() {
		musicTable.vaciar();
		for(Song s:Song.find(null)) {
			Object[] fila = {s.id_song,s.name,MusicGroup.load(s.id_group).name,Gen.load(s.id_gen).name,s.duration};
			musicTable.modelo.addRow(fila);
			
		}
		adM.revalidate();
		adM.repaint();
	}
	
	private void Find() {
		musicTable.vaciar();
		if(adM.tfFind.getText().isEmpty()) {
			LoadSongs();
		}else {
			for(Song s:Song.find(adM.tfFind.getText())) {
				Object[] fila = {s.id_song,s.name,MusicGroup.load(s.id_group).name,Gen.load(s.id_gen).name,s.duration};
				musicTable.modelo.addRow(fila);
				
			}
			adM.revalidate();
			adM.repaint();
		}
		
	}
	
}
