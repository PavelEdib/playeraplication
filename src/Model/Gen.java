package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import Other.Conexion;
import Other.Datos;

public class Gen {
	 public int id_gen;
	 public String name;
	 
	 private Gen(int id, String nm) {
		 this.id_gen = id;
		 this.name = nm;
	 }
	 
	 public static Gen Create(String nm) {
		 	Conexion.open();
		 	
		 	PreparedStatement stm;
		 	ResultSet result;
		 	String insert ="Insert into gen(name) values(?);";
		 	
		 	try {
				stm = Conexion.con.prepareStatement(insert,Statement.RETURN_GENERATED_KEYS);
				stm.setString(1, nm);
				
				stm.executeUpdate();
				result = stm.getGeneratedKeys();
				
				if(result.next()) 
					return load(result.getInt(1));
				
			} catch (SQLException ex) {
				if (Datos.debug) ex.printStackTrace();
				Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
			}
		 
		 return null;
	 }

	public static Gen load(int id) {
		Conexion.open();
		Gen g=null;
		PreparedStatement stm;
		ResultSet result;
		
		String Select ="Select * from gen where id_gen = ? ";
		
		try {
			stm = Conexion.con.prepareStatement(Select);
			stm.setInt(1, id);
			result = stm.executeQuery();
			
			if(result.next()) {
				g = new Gen(result.getInt("id_gen"),
							result.getString("name"));
			}
			stm.close();
			
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
		
		return g;
	}
	
	public static LinkedList<Gen> All(){
		LinkedList<Gen> tempList = new LinkedList<>();
		
		PreparedStatement  stm;
		ResultSet result;
		
		String Select = "Select * from Gen;";
		
		try {
			stm = Conexion.con.prepareStatement(Select);
			result = stm.executeQuery();
			while(result.next()) {
				tempList.add(new Gen(result.getInt("id_gen"),
									result.getString("name")));
			}
			
			stm.close();
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
		
		return tempList;
	}
	
	public int Delete() {
		return Gen.Delete(this.id_gen);
	}

	public static int Delete(int id) {
		Conexion.open();
		PreparedStatement stm;
		try {
			stm=Conexion.con.prepareStatement("delete from gen where id_gen=?");
			stm.setInt(1, id);
			stm.executeUpdate();
			stm.close();
			return 0;
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		}
	}
	
	public String toString() {
		return this.name;
	}
}
