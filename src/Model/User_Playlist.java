package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import com.mysql.jdbc.Statement;

import Other.Conexion;
import Other.Datos;

public class User_Playlist {
	public int id_Playlist;
	public String name;
	public int id_acces;
	public int id_user;
	
	private  User_Playlist(int idP, String nm, int acces,int idUser) {
		this.id_Playlist = idP;
		this.name = nm;
		this.id_acces = acces;
		this.id_user = idUser;
	}
	
	public static User_Playlist Create(String nm, int acces, int idUser) {
		Conexion.open();
		PreparedStatement stm;
		ResultSet result;
		String insert = "Insert into user_playlist(Name, id_acces,id_user) values(?,?,?);";
		
		try {
			stm = Conexion.con.prepareStatement(insert,Statement.RETURN_GENERATED_KEYS);
			stm.setString(1, nm);
			stm.setInt(2, acces);
			stm.setInt(3, idUser);
			
			stm.executeUpdate();
			result = stm.getGeneratedKeys();
			
			if(result.next()) {
				return User_Playlist.load(result.getInt(1));
			}
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		
		
		return null;
	}

	public static User_Playlist load(int id) {
		
		Conexion.open();
		User_Playlist play = null;
		PreparedStatement stm;
		ResultSet result;
		String select="Select * from user_playlist where id_Playlist = ?";
		
		try {
			stm = Conexion.con.prepareStatement(select);
			stm.setInt(1, id);
			
			result = stm.executeQuery();
			if(result.next()) {
				play = new User_Playlist(result.getInt("id_Playlist"),
						                 result.getString("Name"), 
						                 result.getInt("id_acces"), 
						                 result.getInt("id_user"));
			}
			stm.close();
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		
		
		return play;
	}
	
	
	public static LinkedList<User_Playlist> find(int id_user){
		LinkedList<User_Playlist> tempList = new LinkedList<User_Playlist>();
		Conexion.open();
		PreparedStatement stm;
		ResultSet result;
		String select = "Select * from user_playlist where id_user = ?";
		
		try {
			stm = Conexion.con.prepareStatement(select);
			stm.setInt(1, id_user);
			
			result = stm.executeQuery();
			
			while(result.next()) {
				tempList.add(new User_Playlist(result.getInt("id_Playlist"),
											   result.getString("Name"), 
											   result.getInt("id_acces"), 
											   result.getInt("id_user")));
			}
			stm.close();
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		
		
		return tempList;
	}
	
	
	public int Delete() {
		return User_Playlist.Delete(this.id_Playlist);
	}

	public static int Delete(int id) {
		Conexion.open();
		PreparedStatement stm;
		try {
			stm=Conexion.con.prepareStatement("delete from user_playlist where id_Playlist=?");
			stm.setInt(1, id);
			stm.executeUpdate();
			stm.close();
			return 0;
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		}
	}
	
	/*-----Methods into table Song_Playlist -----*/
	
	public int addSong(int idS) {
		
		Conexion.open();
		PreparedStatement stm;
		String insert = "insert into song_playlist(id_song,id_playlist) values(?,?);";
		
		try {
			stm = Conexion.con.prepareStatement(insert);
			stm.setInt(1, idS);
			stm.setInt(2, this.id_Playlist);
			
			stm.executeUpdate();
			
			stm.close();
			return 0;
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		}
		
	}
	
	
	//get all Songs from this Playlist
	public LinkedList<Song> GetSongs() {
		LinkedList<Song> tempList = new LinkedList<Song>();
		Conexion.open();
		PreparedStatement stm;
		ResultSet result;
		
		String select = "Select * from song_playlist where id_playlist = ?";
		
		try {
			stm = Conexion.con.prepareStatement(select);
			stm.setInt(1, this.id_Playlist);
			
			result = stm.executeQuery();
			
			while(result.next()) {
				tempList.add(Song.load(result.getInt("id_song")));
			}
			
			stm.close();
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		
		
		return tempList;
	}
	
	public int DeleteSongs(int idS) {
		
		Conexion.open();
		PreparedStatement stm;
		try {
			stm=Conexion.con.prepareStatement("delete from song_playlist where id_playlist=? and id_song = ?");
			stm.setInt(1, this.id_Playlist);
			stm.setInt(2, idS);
			stm.executeUpdate();
			stm.close();
			return 0;
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		
		
		}
	}
	
	public String toString() {
		return this.name;
	}
	
	
}
