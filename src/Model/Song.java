package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import com.mysql.jdbc.Statement;

import Other.Conexion;
import Other.Datos;

public class Song {
	public int id_song;
	public String name;
	public int id_group;
	public int id_gen;
	public String duration;
	
	
	private Song(int idS,String nm,int idg,int idgen,String dur) {
		this.id_song = idS;
		this.name = nm;
		this.id_group = idg;
		this.id_gen = idgen;
		this.duration = dur;
	}
	
	public static Song Create(String name,int idg,int idgen,String dur) {
		
		Conexion.open();
		PreparedStatement stm;
		ResultSet result;
		String insert = "Insert into song(name,id_group,id_gen,duration) values(?,?,?,?);";
		
		try {
			stm = Conexion.con.prepareStatement(insert,Statement.RETURN_GENERATED_KEYS);
			stm.setString(1, name);
			stm.setInt(2, idg);
			stm.setInt(3,idgen);
			stm.setString(4, dur);
			
			stm.executeUpdate();
			result = stm.getGeneratedKeys();
			
			if(result.next()) {
				return load(result.getInt(1));
			}
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		
		
		
		return null;
	}

	public static Song load(int id) {
		Conexion.open();
		Song s = null;
		PreparedStatement stm;
		ResultSet result;
		String select = "Select * from Song where id_song = ?";
		
		try {
			stm = Conexion.con.prepareStatement(select);
			stm.setInt(1, id);
			result = stm.executeQuery();
			if(result.next()) {
				s = new Song(result.getInt("id_song"),
							 result.getString("name"),
							 result.getInt("id_group"),
							 result.getInt("id_gen"),
							 result.getString("duration"));
			}
			stm.close();
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}

		return s;
	}
	
	public static LinkedList<Song> find(String name){
		LinkedList<Song> tempList = new LinkedList<>();
		Conexion.open();
		PreparedStatement stm;
		ResultSet result;
		String Select = "Select * from song where 1=1 ";
		if(name!=null) Select+=" and name like ?";
		
		try {
			stm = Conexion.con.prepareStatement(Select);
			if(name!=null) stm.setString(1, name+"%");
			
			result = stm.executeQuery();
			while(result.next()) {
				tempList.add(new Song(result.getInt("id_song"),
						 			  result.getString("name"),
						 			  result.getInt("id_group"),
						 			  result.getInt("id_gen"),
						 			  result.getString("duration")));
			}
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		
		
		
		return tempList;
	}
	
	public int Delete() {
		return Song.Delete(this.id_song);
	}

	public static int Delete(int id) {
		Conexion.open();
		PreparedStatement stm;
		try {
			stm=Conexion.con.prepareStatement("delete from song where id_song=?");
			stm.setInt(1, id);
			stm.executeUpdate();
			stm.close();
			return 0;
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		}
		
	}
	
	public void Update() {
		Conexion.open();
		PreparedStatement stm;
		String update = "Update song Set name = ?,id_group = ?,id_gen = ?,duration = ? where id_song = ?";
		
		try {
			stm = Conexion.con.prepareStatement(update);
			stm.setString(1, this.name);
			stm.setInt(2, this.id_group);
			stm.setInt(3, this.id_gen);
			stm.setString(4, this.duration);
			stm.setInt(5, this.id_song);
			
			stm.executeUpdate();
			stm.close();
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
	}
	
	//Get all Songs Using user id
	public static LinkedList<Song> getAllSongs(int idUser){
		LinkedList<Song> tempList = new LinkedList<>();
		
		Conexion.open();
		PreparedStatement stm;
		ResultSet result;
		String Select = "SELECT DISTINCT(id_song) FROM song_playlist WHERE id_playlist in (SELECT id_playlist From user_playlist WHERE id_user = ?);";
		
		try {
			stm = Conexion.con.prepareStatement(Select);
			stm.setInt(1, idUser);
			result = stm.executeQuery();
			
			while(result.next()) {
				tempList.add(Song.load(result.getInt("id_song")));
			}
			stm.close();
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		
		return tempList;
	}
	
	
	
}
