package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import com.sun.net.httpserver.Authenticator.Result;

import Other.Conexion;
import Other.Datos;
import Other.Fecha;

public class User {
 public int id_user;
 public String last_name;
 public String first_name;
 public String mail;
 public String PhoneN;
 public Fecha BirthDate;
 public String username;
 public String password;
 
 private User(int id, String fname, String lname,String em,String phone,Fecha BirthD,String user,String pass) {
	 this.id_user = id;
	 this.first_name = fname;
	 this.last_name = lname;
	 this.mail = em;
	 this.PhoneN = phone;
	 this.BirthDate = BirthD;
	 this.username = user;
	 this.password = pass;
 }
 
 public static User Create(String fname, String lname,String em,String phone,Fecha BirthD,String user,String pass) {
	 
	 Conexion.open();
	 PreparedStatement stm;
	 ResultSet result;
	 String insert ="Insert into User(first_name,last_name,mail,PhoneN,BirthDate,username,password) values(?,?,?,?,?,?,?);";
	 
	 try {
		stm = Conexion.con.prepareStatement(insert,Statement.RETURN_GENERATED_KEYS);
		stm.setString(1,fname);
		stm.setString(2, lname);
		stm.setString(3, em);
		stm.setString(4, phone);
		stm.setDate(5, BirthD.getFechaMysql());
		stm.setString(6, user);
		stm.setString(7, pass);
		
		stm.executeUpdate();
		result = stm.getGeneratedKeys();
		
		if(result.next()) 
			User.Priv(result.getInt(1));
			return load(result.getInt(1));
		
		
	} catch (SQLException ex) {
		if (Datos.debug) ex.printStackTrace();
		Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
	}
	 
	 
	 return null;
 }
 
 
	

	 	public static User load(int id) {
			Conexion.open();
			PreparedStatement stm;
			ResultSet result;
			User u=null;
			String Select = "Select * from user where id_user = ?";
			
			try {
				stm = Conexion.con.prepareStatement(Select);
				stm.setInt(1, id);
				
				result= stm.executeQuery();
				
				if(result.next()) {
					
					u = new User(result.getInt("id_user"),
								 result.getString("first_name"),
								 result.getString("last_name"),
								 result.getString("mail"),
								 result.getString("PhoneN"),
								 Fecha.toFecha(result.getDate("BirthDate")),
								 result.getString("username"),
								 result.getString("password"));
					
				}
				
				stm.close();
				
			} catch (SQLException ex) {
				if (Datos.debug) ex.printStackTrace();
				Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
			}
			
			
			return u;
	}
	 
	 	
	
	public static LinkedList<User> find(String username,String mail){
		LinkedList<User> tempList = new LinkedList<User>();
		Conexion.open();
		int x= 1;
		PreparedStatement stm;
		ResultSet result;
		String select = "Select * from user where 1=1 ";
		if(username!=null) select+= "and username Like ? ";
		if(mail!=null) select+= "and email = ?"; 
		
		try {
			
			stm = Conexion.con.prepareStatement(select);
			if(username!=null) {stm.setString(x, username); x++;}
			if(mail!=null) {stm.setString(x, mail);} 
			result = stm.executeQuery();
			
			while(result.next()) {
				tempList.add(new User(result.getInt("id_user"),
							 result.getString("first_name"),
							 result.getString("last_name"),
							 result.getString("mail"),
							 result.getString("PhoneN"),
							 Fecha.toFecha(result.getDate("BirthDate")),
							 result.getString("username"),
							 result.getString("password"))
							);
			}
			stm.close();
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
		return tempList;
	}
	
	public int delete() {
		return User.delete(this.id_user);
	}
	
	
	public static int delete(int i) {
		Conexion.open();
		PreparedStatement stm;
		try {
			stm=Conexion.con.prepareStatement("delete from user where actor_id=?");
			stm.setInt(1, i);
			stm.executeUpdate();
			stm.close();
			return 0;
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		}
		
	}
	
	public void update() {
		Conexion.open();
		PreparedStatement stm;
		
		try {
			stm=Conexion.con.prepareStatement("Update User set password=? where id_user=?");
			stm.setString(1,this.password);
			stm.setInt(2,this.id_user);
			stm.executeUpdate();
			
			stm.close();
			
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		
	}
	
	
	//----Methods for User_priv
	
	private static void Priv(int id) {
		Conexion.open();
		 PreparedStatement stm;
		 String insert ="Insert into user_priv(id_user,priv) Values(?,?);";
		 
		 try {
			stm = Conexion.con.prepareStatement(insert);
			stm.setInt(1, id);
			stm.setInt(2, 1);
			stm.executeUpdate();
			
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
	}
	
	public int getPriv() {
		
		Conexion.open();
		 PreparedStatement stm;
		 ResultSet result;
		 String select ="Select * from user_priv where id_user = ?";
		 
		 try {
			stm = Conexion.con.prepareStatement(select);
			stm.setInt(1,this.id_user);
			
			result = stm.executeQuery();
			
			if(result.next()) {
				return result.getInt("priv");
			}
			
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
		 
		return 0;
	}
	
	public void setPriv(int priv) {
		Conexion.open();
		 PreparedStatement stm;
		 String update ="Update user_priv set priv = ? where id_user = ?";
		 
		 try {
			stm = Conexion.con.prepareStatement(update);
			stm.setInt(1, priv);
			stm.setInt(2, this.id_user);
			
			stm.executeUpdate();
			
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
	}
	
	
	public LinkedList<User> getFriend(){
		LinkedList<User> tempList = new LinkedList<User>();
		Conexion.open();
		PreparedStatement stm;
		ResultSet result;
		String Select = "Select id_folow from folow where id_user = ?";
		
		try {
			stm = Conexion.con.prepareStatement(Select);
			stm.setInt(1, this.id_user);
			result = stm.executeQuery();
			while(result.next()) {
				tempList.add(User.load(result.getInt("id_folow")));
			}
			stm.close();
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
		
			
		return tempList;
	}
	
	public void ADDFriend(int idFriend) {
		Conexion.open();
		PreparedStatement stm;
		String insert = "Insert into folow(id_user,id_folow) values(?,?);";
		
		try {
			stm = Conexion.con.prepareStatement(insert);
			stm.setInt(1, this.id_user);
			stm.setInt(2, idFriend);
			stm.executeUpdate();
			stm.close();
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
		
	}
	
	public void RemoveFriend(int idFriend) {
		Conexion.open();
		PreparedStatement stm;
		String delete = "Delete From folow where id_user = ? and id_folow = ?";
		
		try {
			stm = Conexion.con.prepareStatement(delete);
			stm.setInt(1, this.id_user);
			stm.setInt(2, idFriend);
			stm.executeUpdate();
			stm.close();
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
	}
	
	public boolean isFriend(int idFriend) {
		boolean an = false;
		
		for(User u:this.getFriend()) {
			if(u.id_user == idFriend) {
				an = true;
				break;
				}
		}
		
		return an;
	}
	
 
 
}
