package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import Other.Conexion;
import Other.Datos;

public class MusicGroup {
	public int id_group;
	public String name;
	
	private MusicGroup(int id, String nm) {
		this.id_group = id;
		this.name = nm;
	}
	
	 public static MusicGroup Create(String nm) {
		 	Conexion.open();
		 	
		 	PreparedStatement stm;
		 	ResultSet result;
		 	String insert ="Insert into musicgroup(name) values(?);";
		 	
		 	try {
				stm = Conexion.con.prepareStatement(insert,Statement.RETURN_GENERATED_KEYS);
				stm.setString(1, nm);
				
				stm.executeUpdate();
				result = stm.getGeneratedKeys();
				
				if(result.next()) 
					return load(result.getInt(1));
				
			} catch (SQLException ex) {
				if (Datos.debug) ex.printStackTrace();
				Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
			}
		 
		 return null;
	 }

	public static MusicGroup load(int id) {
		Conexion.open();
		MusicGroup mg=null;
		PreparedStatement stm;
		ResultSet result;
		
		String Select ="Select * from musicgroup where id_group = ? ";
		
		try {
			stm = Conexion.con.prepareStatement(Select);
			stm.setInt(1, id);
			result = stm.executeQuery();
			
			if(result.next()) {
				mg = new MusicGroup(result.getInt("id_group"),
							result.getString("Name"));
			}
			stm.close();
			
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
		
		return mg;
	}
	
	public static LinkedList<MusicGroup> find(String nm){
		LinkedList<MusicGroup> tempList = new LinkedList<MusicGroup>();
		
		PreparedStatement stm;
		ResultSet result;
		
		String Select = "Select * from MusicGroup Where 1=1";
		if(nm!=null) Select+=" and Name like ?";
		
		try {
			stm= Conexion.con.prepareStatement(Select);
			if(nm!=null) stm.setString(1, nm+"%");
			
			result = stm.executeQuery();
			
			while(result.next()) {
				tempList.add(new MusicGroup(result.getInt("id_group"),
											result.getString("Name")));
			}
			
			stm.close();
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
		
		
		return tempList;
	}
	
	public int Delete() {
		return Gen.Delete(this.id_group);
	}

	public static int Delete(int id) {
		Conexion.open();
		PreparedStatement stm;
		try {
			stm=Conexion.con.prepareStatement("delete from musicgroup where id_group=?");
			stm.setInt(1, id);
			stm.executeUpdate();
			stm.close();
			return 0;
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		}
		
	}
	
	public String toString() {
		return this.name;
	}
	
}
