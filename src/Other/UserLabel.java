package Other;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;



public class UserLabel extends JLabel {
	
	public int idUser;
	
	public UserLabel(String name,int x ,int y,int idU) {
		super("   "+name);
		this.idUser = idU;
		
		
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				setBounds(x-9, y-6, 690, 150);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				setBounds(x, y, 669, 132);
			}
			@Override
			public void mousePressed(MouseEvent e) {
				setBackground(new Color(0, 255, 0));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				setBackground(new Color(152, 251, 152));
			}
		});
		this.setOpaque(true);
		this.setBackground(new Color(152, 251, 152));
		this.setFont(new Font("Segoe UI Semibold", Font.BOLD, 40));
		this.setIcon(new ImageIcon(UserLabel.class.getResource("/Img/user.png")));
		this.setBounds(x, y, 669, 133);
	}
	
	
}
