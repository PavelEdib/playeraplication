package Other;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class SongLabel extends JLabel {
	
	public int id;
	public Integer idPlaylist;
	public Integer idUser;
	
	public SongLabel(String name, int x,int y,int idS,Integer idP,Integer idUser) {
		super(name);
		this.id = idS;
		this.idPlaylist= idP;
		this.idUser = idUser;
		this.addMouseListener(new MouseAdapter() {
			
			
			@Override
			public void mouseEntered(MouseEvent e) {
				setBounds(x-9, y-6, 690, 71);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				setBounds(x, y, 669, 53);
			}
			@Override
			public void mousePressed(MouseEvent e) {
				setBackground(new Color(0, 128, 128));
			}
			
			@Override
			public void mouseReleased(MouseEvent e) {
				setBackground(new Color(25, 25, 112));
			}
		});
		this.setForeground(new Color(0, 255, 0));
		this.setBackground(new Color(25, 25, 112));
		this.setOpaque(true);
		this.setHorizontalAlignment(SwingConstants.CENTER);
		this.setFont(new Font("Segoe UI Semibold", Font.BOLD, 22));
		this.setBounds(x, y, 669, 53); //x=29 constant && y+=70; 
		
	}

}
