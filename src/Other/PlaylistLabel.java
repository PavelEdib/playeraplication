package Other;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;



public class PlaylistLabel extends JLabel {
	
	public int id;
	
	public PlaylistLabel(String name,int x,int y,int idP) {
		super("  "+name);
		this.id = idP;
		
		 
		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				setBounds(x-9, y-6, 690, 101);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				setBounds(x, y, 669, 83);
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				setBackground(new Color(255, 165, 0));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				setBackground(new Color(255, 215, 0));
			}
		});
		this.setOpaque(true);
		this.setBackground(new Color(255, 215, 0));
		this.setForeground(new Color(139, 0, 139));
		this.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		this.setIcon(new ImageIcon(PlaylistLabel.class.getResource("/Img/PlayList.png")));
		this.setHorizontalAlignment(SwingConstants.CENTER);
		this.setBounds(x, y, 669, 83);
	}
	

}
