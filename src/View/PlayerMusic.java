package View;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import java.awt.Font;

public class PlayerMusic extends JPanel {

	public JLabel lblTextMusic;
	public JLabel lblUndo;
	public JLabel lblAdd;
	public JLabel lblPlay;
	public JLabel lblStop;
	public JLabel lblPause;

	/**
	 * Create the panel.
	 */
	public PlayerMusic() {
		setLayout(null);
		setBounds(265, 0, 738, 707);
		//setPreferredSize(new Dimension(738,707));
		
		lblUndo = new JLabel("");
		lblUndo.setHorizontalAlignment(SwingConstants.CENTER);
		lblUndo.setIcon(new ImageIcon(PlayerMusic.class.getResource("/Img/undo.png")));
		lblUndo.setBounds(10, 10, 72, 52);
		add(lblUndo);
		
		lblAdd = new JLabel("");
		lblAdd.setHorizontalAlignment(SwingConstants.CENTER);
		lblAdd.setIcon(new ImageIcon(PlayerMusic.class.getResource("/Img/plus.png")));
		lblAdd.setBounds(621, 602, 88, 79);
		add(lblAdd);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setIcon(new ImageIcon(PlayerMusic.class.getResource("/Img/song.png")));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(152, 114, 450, 398);
		add(lblNewLabel_2);
		
		lblTextMusic = new JLabel("Thousand Foot Krutch: I Get Wicked");
		lblTextMusic.setHorizontalAlignment(SwingConstants.CENTER);
		lblTextMusic.setFont(new Font("Segoe UI Semibold", Font.BOLD, 20));
		lblTextMusic.setBounds(102, 538, 559, 40);
		add(lblTextMusic);
		
		lblPlay = new JLabel("");
		lblPlay.setIcon(new ImageIcon(PlayerMusic.class.getResource("/Img/play-button.png")));
		lblPlay.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlay.setBounds(328, 602, 88, 67);
		add(lblPlay);
		
		lblStop = new JLabel("");
		lblStop.setIcon(new ImageIcon(PlayerMusic.class.getResource("/Img/btn-stop.png")));
		lblStop.setHorizontalAlignment(SwingConstants.CENTER);
		lblStop.setBounds(426, 602, 88, 67);
		add(lblStop);
		
		lblPause = new JLabel("");
		lblPause.setIcon(new ImageIcon(PlayerMusic.class.getResource("/Img/btn-pause.png")));
		lblPause.setHorizontalAlignment(SwingConstants.CENTER);
		lblPause.setBounds(228, 602, 88, 67);
		add(lblPause);
		
		repaint();
		revalidate();
		
	}

}
