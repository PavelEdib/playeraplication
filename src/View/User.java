package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class User extends JFrame {

	private JPanel contentPane;
	public JTextField tfUsername;
	public JTextField tfPass;
	public JSpinner spinnerPriv;
	public JButton btnSave;
	public JButton btnDelete;
	public JLabel lblUser;

	/**
	 * Create the frame.
	 */
	public User() {
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblUser = new JLabel("New label");
		lblUser.setHorizontalAlignment(SwingConstants.CENTER);
		lblUser.setFont(new Font("Sitka Display", Font.BOLD, 18));
		lblUser.setBounds(103, 10, 242, 33);
		contentPane.add(lblUser);
		
		JLabel lblNewLabel = new JLabel("Username:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setFont(new Font("Sitka Display", Font.BOLD, 18));
		lblNewLabel.setBounds(10, 75, 134, 33);
		contentPane.add(lblNewLabel);
		
		tfUsername = new JTextField();
		tfUsername.setFont(new Font("Sitka Display", Font.BOLD, 17));
		tfUsername.setEditable(false);
		tfUsername.setBounds(154, 75, 242, 33);
		contentPane.add(tfUsername);
		tfUsername.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPassword.setFont(new Font("Sitka Display", Font.BOLD, 18));
		lblPassword.setBounds(10, 118, 134, 33);
		contentPane.add(lblPassword);
		
		tfPass = new JTextField();
		tfPass.setFont(new Font("Sitka Display", Font.BOLD, 15));
		tfPass.setColumns(10);
		tfPass.setBounds(154, 118, 242, 33);
		contentPane.add(tfPass);
		
		JLabel lblPrivelegia = new JLabel("Privelegia:");
		lblPrivelegia.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPrivelegia.setFont(new Font("Sitka Display", Font.BOLD, 18));
		lblPrivelegia.setBounds(10, 166, 134, 33);
		contentPane.add(lblPrivelegia);
		
		spinnerPriv = new JSpinner();
		spinnerPriv.setModel(new SpinnerNumberModel(0, 0, 3, 1));
		spinnerPriv.setFont(new Font("Sitka Display", Font.BOLD, 17));
		spinnerPriv.setBounds(154, 161, 56, 33);
		contentPane.add(spinnerPriv);
		
		btnSave = new JButton("Save");
		btnSave.setFont(new Font("Segoe UI Semibold", Font.BOLD, 17));
		btnSave.setBounds(147, 204, 102, 49);
		contentPane.add(btnSave);
		
		btnDelete = new JButton("Delete");
		btnDelete.setFont(new Font("Segoe UI Semibold", Font.BOLD, 17));
		btnDelete.setBounds(260, 204, 107, 49);
		contentPane.add(btnDelete);
	}
}
