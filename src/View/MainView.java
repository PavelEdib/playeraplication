package View;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import javax.swing.JScrollPane;


import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.UIManager;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JButton;

public class MainView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JPanel contentPane;
	public JLabel lblLogOut;
	public JLabel lblFriends;
	public JLabel lblPlaylists;
	public JLabel lblMusic;
	public JMenu mnAdmin;
	public JMenuItem mntmSongs;
	public JMenuItem mntmGroup;
	public JMenuItem mntmGen;

	public JPanel Panel_PopularNow;

	public  JScrollPane scrollPaneMain;
	
	//panel Header
	public JTextField tfSearch;
	public JButton btnFind;
	public JPanel panelHeader;

	public JScrollPane scrollPane;
	public JLabel lblPlay;
	
	//panel Header User
	public JTextField tfFinduser;
	public JPanel panelHeaderUser;
	public JButton btnFindUser;

	public JPanel panelUserProfile;

	public JLabel lblNameUser;

	public JButton btnFollow;

	
	
	/**
	 * Create the frame.
	 */
	public MainView() {
		setResizable(false);
		this.setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1013, 760);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnAdmin = new JMenu("Admin menu");
		menuBar.add(mnAdmin);
		
		mntmSongs = new JMenuItem("Control the Songs");
		mnAdmin.add(mntmSongs);
		
		mntmGroup = new JMenuItem("Control the Users");
		mnAdmin.add(mntmGroup);
		
	
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 266, 707);
		contentPane.add(scrollPane);
		
		JPanel panel = new JPanel();
		panel.setForeground(Color.BLACK);
		scrollPane.setViewportView(panel);
		panel.setLayout(null);
		
		lblLogOut = new JLabel("Log Out");
		lblLogOut.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				lblLogOut.setBounds(2, 409, 260, 75);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblLogOut.setBounds(10, 418, 244, 61);
			}
			@Override
			public void mousePressed(MouseEvent e) {
				lblLogOut.setBackground(new Color(75, 0, 130));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				lblLogOut.setBackground(new Color(128, 0, 128));
			}
		});
		lblLogOut.setOpaque(true);
		lblLogOut.setBackground(new Color(128, 0, 128));
		lblLogOut.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogOut.setForeground(Color.GREEN);
		lblLogOut.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblLogOut.setBounds(10, 418, 244, 61);
		panel.add(lblLogOut);
		
		lblFriends = new JLabel("Friends");
		lblFriends.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				lblFriends.setBounds(2, 328, 260, 75);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblFriends.setBounds(10, 337, 244, 61);
			}
			@Override
			public void mousePressed(MouseEvent e) {
				lblFriends.setBackground(new Color(75, 0, 130));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				lblFriends.setBackground(new Color(128, 0, 128));
			}
		});
		lblFriends.setOpaque(true);
		lblFriends.setHorizontalAlignment(SwingConstants.CENTER);
		lblFriends.setForeground(Color.GREEN);
		lblFriends.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblFriends.setBackground(new Color(128, 0, 128));
		lblFriends.setBounds(10, 337, 244, 61);
		panel.add(lblFriends);
		
		lblPlaylists = new JLabel("Playlists");
		lblPlaylists.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				lblPlaylists.setBounds(2, 250, 260, 75);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblPlaylists.setBounds(10, 259, 244, 61);
			}
			@Override
			public void mousePressed(MouseEvent e) {
				lblPlaylists.setBackground(new Color(75, 0, 130));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				lblPlaylists.setBackground(new Color(128, 0, 128));
			}
		});
		//Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		lblPlaylists.setOpaque(true);
		lblPlaylists.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlaylists.setForeground(Color.GREEN);
		lblPlaylists.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblPlaylists.setBackground(new Color(128, 0, 128));
		lblPlaylists.setBounds(10, 259, 244, 61);
		panel.add(lblPlaylists);
		
		lblMusic = new JLabel("Popular Now");
		lblMusic.setOpaque(true);
		lblMusic.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				lblMusic.setBounds(2, 165, 260, 75);
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblMusic.setBounds(10, 174, 244, 61);
			}
			@Override
			public void mousePressed(MouseEvent e) {
				lblMusic.setBackground(new Color(75, 0, 130));
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				lblMusic.setBackground(new Color(128, 0, 128));
			}
		});
		lblMusic.setHorizontalAlignment(SwingConstants.CENTER);
		lblMusic.setForeground(Color.GREEN);
		lblMusic.setFont(new Font("Times New Roman", Font.BOLD, 18));
		lblMusic.setBackground(new Color(128, 0, 128));
		lblMusic.setBounds(10, 174, 244, 61);
		panel.add(lblMusic);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(MainView.class.getResource("/Img/MenuBack.png")));
		lblNewLabel_1.setBounds(0, 0, 264, 705);
		panel.add(lblNewLabel_1);
		
		scrollPaneMain = new JScrollPane();
		scrollPaneMain.setBounds(265, 0, 738, 707);
		contentPane.add(scrollPaneMain);
		
		Panel_PopularNow = new JPanel();
		Panel_PopularNow.setBackground(new Color(176, 196, 222));
		scrollPaneMain.setViewportView(Panel_PopularNow);
		Panel_PopularNow.setPreferredSize(new Dimension(710,900));
		Panel_PopularNow.setLayout(null);
		
		//header 
		panelHeader = new JPanel();
		panelHeader.setBackground(new Color(128, 0, 128));
		panelHeader.setBounds(0, 0, 710, 97);
		Panel_PopularNow.add(panelHeader);
		panelHeader.setLayout(null);
		
		tfSearch = new JTextField();
		tfSearch.setFont(new Font("Sitka Display", Font.BOLD, 15));
		tfSearch.setColumns(10);
		tfSearch.setBounds(270, 10, 280, 45);
		panelHeader.add(tfSearch);
		
		btnFind = new JButton("");
		btnFind.setIcon(new ImageIcon(MainView.class.getResource("/Img/find.png")));
		btnFind.setBounds(583, 10, 85, 45);
		panelHeader.add(btnFind);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(Color.GREEN);
		separator.setBounds(0, 79, 718, 2);
		panelHeader.add(separator);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(MainView.class.getResource("/Img/gradient-background-1200x333.jpg")));
		lblNewLabel.setBounds(0, 0, 730, 97);
		panelHeader.add(lblNewLabel);
		
		//panel User Profile
		panelUserProfile = new JPanel();
		panelUserProfile.setBounds(0, 0, 719, 177);
		//Panel_PopularNow.add(panelUserProfile);
		panelUserProfile.setLayout(null);
		
		lblNameUser = new JLabel("Jechiu Pavel");
		lblNameUser.setForeground(Color.RED);
		lblNameUser.setHorizontalAlignment(SwingConstants.CENTER);
		lblNameUser.setFont(new Font("Segoe UI Semibold", Font.BOLD, 30));
		lblNameUser.setBounds(245, 48, 324, 66);
		panelUserProfile.add(lblNameUser);
		
		JLabel lblImg = new JLabel("");
		lblImg.setIcon(new ImageIcon(MainView.class.getResource("/Img/user.png")));
		lblImg.setHorizontalAlignment(SwingConstants.CENTER);
		lblImg.setBounds(20, 10, 148, 137);
		panelUserProfile.add(lblImg);
		
		btnFollow = new JButton("Follow");
		btnFollow.setFont(new Font("Sitka Display", Font.BOLD, 15));
		btnFollow.setBounds(608, 121, 101, 34);
		panelUserProfile.add(btnFollow);
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setIcon(new ImageIcon(MainView.class.getResource("/Img/gradient-background-1200x333.jpg")));
		lblNewLabel_3.setBounds(0, 0, 719, 177);
		panelUserProfile.add(lblNewLabel_3);
		
		//Header panel for Users
		panelHeaderUser = new JPanel();
		panelHeaderUser.setOpaque(false);
		panelHeaderUser.setLayout(null);
		panelHeaderUser.setBackground(new Color(128, 0, 128));
		panelHeaderUser.setBounds(0, 0, 710, 97);
		//Panel_PopularNow.add(panelHeaderUser);
		
		btnFindUser = new JButton("");
		btnFindUser.setIcon(new ImageIcon(MainView.class.getResource("/Img/find.png")));
		btnFindUser.setBounds(583, 10, 85, 45);
		panelHeaderUser.add(btnFindUser);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBackground(Color.GREEN);
		separator_1.setBounds(0, 79, 718, 2);
		panelHeaderUser.add(separator_1);
		
		tfFinduser = new JTextField();
		tfFinduser.setFont(new Font("Sitka Display", Font.BOLD, 15));
		tfFinduser.setColumns(10);
		tfFinduser.setBounds(258, 10, 280, 45);
		panelHeaderUser.add(tfFinduser);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setBounds(0, 0, 730, 97);
		panelHeaderUser.add(lblNewLabel_2);
		lblNewLabel_2.setIcon(new ImageIcon(MainView.class.getResource("/Img/gradient-background-1200x333.jpg")));
		lblNewLabel_2.setOpaque(true);
		
		
		lblPlay = new JLabel("PlayLists");
		lblPlay.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlay.setForeground(new Color(0, 0, 128));
		lblPlay.setFont(new Font("Segoe UI Semibold", Font.BOLD, 75));
		lblPlay.setBounds(110, 20, 555, 96);
	}
}
