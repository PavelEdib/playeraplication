package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Other.Tabla;

import javax.swing.JScrollPane;
import java.awt.FlowLayout;
import javax.swing.JMenuBar;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.ScrollPane;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;
import javax.swing.ImageIcon;

public class AdminControlView extends JFrame {
	public Tabla musicTable;
	public JLabel lblNewSong;
	public JScrollPane scrollPane;
	public JPanel panelHeader;
	public JTextField tfFind;
	public JButton btnFind;

	/**
	 * Create the frame.
	 */
	public AdminControlView() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		setBounds(100, 100, 590, 676);
		getContentPane().setLayout(null);
		getContentPane().setLayout(null);
		
		panelHeader = new JPanel();
		panelHeader.setBounds(0, 0, 586, 42);
		getContentPane().add(panelHeader);
		panelHeader.setLayout(null);
		
		lblNewSong = new JLabel("New Song");
		lblNewSong.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				lblNewSong.setBackground(new Color(0, 128, 0));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblNewSong.setBackground(Color.GREEN);
			}
			@Override
			public void mousePressed(MouseEvent e) {
				lblNewSong.setBackground(Color.RED);
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				lblNewSong.setBackground(Color.GREEN);
			}
		});
		lblNewSong.setForeground(new Color(0, 0, 255));
		lblNewSong.setBackground(new Color(0, 255, 0));
		lblNewSong.setOpaque(true);
		lblNewSong.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewSong.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		lblNewSong.setBounds(231, 0, 355, 41);
		panelHeader.add(lblNewSong);
		
		tfFind = new JTextField();
		tfFind.setFont(new Font("Sitka Display", Font.BOLD, 15));
		tfFind.setBounds(0, 0, 157, 41);
		panelHeader.add(tfFind);
		tfFind.setColumns(10);
		
		btnFind = new JButton("");
		btnFind.setIcon(new ImageIcon(AdminControlView.class.getResource("/Img/find.png")));
		btnFind.setBounds(156, 0, 77, 41);
		panelHeader.add(btnFind);
		
		
		
		
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 43, 586, 605);
		getContentPane().add(scrollPane);
		
	}
	
	
	
}
