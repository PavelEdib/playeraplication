package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import com.toedter.calendar.JDateChooser;
import javax.swing.JPasswordField;

public class Register extends JFrame {

	public JPanel contentPane;
	public JTextField tfFname;
	public JTextField tfLname;
	public JTextField tfMail;
	public JTextField tfPhone;
	public JTextField tfUsername;
	public JDateChooser dateChooser;
	public JButton btnSignUp;
	public JPasswordField tfPass;
	public JPasswordField tfPassRepeate;

	

	/**
	 * Create the frame.
	 */
	public Register() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 535, 480);
		this.setVisible(true);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		tfFname = new JTextField();
		tfFname.setFont(new Font("Times New Roman", Font.BOLD, 14));
		tfFname.setBounds(30, 69, 180, 29);
		contentPane.add(tfFname);
		tfFname.setColumns(10);
		
		tfLname = new JTextField();
		tfLname.setFont(new Font("Times New Roman", Font.BOLD, 14));
		tfLname.setColumns(10);
		tfLname.setBounds(310, 69, 180, 29);
		contentPane.add(tfLname);
		
		tfMail = new JTextField();
		tfMail.setFont(new Font("Times New Roman", Font.BOLD, 14));
		tfMail.setColumns(10);
		tfMail.setBounds(30, 144, 180, 29);
		contentPane.add(tfMail);
		
		tfPhone = new JTextField();
		tfPhone.setFont(new Font("Times New Roman", Font.BOLD, 14));
		tfPhone.setColumns(10);
		tfPhone.setBounds(310, 144, 180, 29);
		contentPane.add(tfPhone);
		
		tfUsername = new JTextField();
		tfUsername.setFont(new Font("Times New Roman", Font.BOLD, 14));
		tfUsername.setColumns(10);
		tfUsername.setBounds(30, 232, 180, 29);
		contentPane.add(tfUsername);
		
		JLabel lblNewLabel = new JLabel("First Name");
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblNewLabel.setBounds(30, 46, 114, 21);
		contentPane.add(lblNewLabel);
		
		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setHorizontalAlignment(SwingConstants.LEFT);
		lblLastName.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblLastName.setBounds(312, 46, 114, 21);
		contentPane.add(lblLastName);
		
		JLabel lblMail = new JLabel("Mail");
		lblMail.setHorizontalAlignment(SwingConstants.LEFT);
		lblMail.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblMail.setBounds(30, 119, 114, 21);
		contentPane.add(lblMail);
		
		JLabel lblMobileNumber = new JLabel("Mobile Number");
		lblMobileNumber.setHorizontalAlignment(SwingConstants.LEFT);
		lblMobileNumber.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblMobileNumber.setBounds(310, 119, 114, 21);
		contentPane.add(lblMobileNumber);
		
		JLabel lblUserName = new JLabel("User Name");
		lblUserName.setHorizontalAlignment(SwingConstants.LEFT);
		lblUserName.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblUserName.setBounds(30, 212, 114, 21);
		contentPane.add(lblUserName);
		
		JLabel lblBirthDate = new JLabel("Birth Date");
		lblBirthDate.setHorizontalAlignment(SwingConstants.LEFT);
		lblBirthDate.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblBirthDate.setBounds(312, 212, 114, 21);
		contentPane.add(lblBirthDate);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setHorizontalAlignment(SwingConstants.LEFT);
		lblPassword.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblPassword.setBounds(30, 304, 114, 21);
		contentPane.add(lblPassword);
		
		JLabel lblRepeatePassword = new JLabel("Repeate Password ");
		lblRepeatePassword.setHorizontalAlignment(SwingConstants.LEFT);
		lblRepeatePassword.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblRepeatePassword.setBounds(310, 304, 138, 21);
		contentPane.add(lblRepeatePassword);
		
		btnSignUp = new JButton("Sign up");
		btnSignUp.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnSignUp.setBounds(210, 382, 114, 34);
		contentPane.add(btnSignUp);
		
		dateChooser = new JDateChooser();
		dateChooser.setBounds(310, 232, 180, 29);
		dateChooser.setDateFormatString("dd-MM-yyyy");
		contentPane.add(dateChooser);
		
		tfPass = new JPasswordField();
		tfPass.setFont(new Font("Times New Roman", Font.BOLD, 14));
		tfPass.setBounds(30, 325, 180, 29);
		contentPane.add(tfPass);
		
		tfPassRepeate = new JPasswordField();
		tfPassRepeate.setFont(new Font("Times New Roman", Font.BOLD, 14));
		tfPassRepeate.setBounds(310, 325, 189, 29);
		contentPane.add(tfPassRepeate);
	}
}
