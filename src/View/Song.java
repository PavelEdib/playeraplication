package View;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Model.Gen;
import Model.MusicGroup;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JButton;

public class Song extends JFrame {

	private JPanel contentPane;
	public JTextField tfName;
	public JTextField tfDuration;
	public JButton btnInsert;
	public JButton btnDelete;
	public JComboBox<MusicGroup> comboBoxGroup;
	public JLabel lblDuration;
	public JComboBox<Gen> comboBoxGen;
	public JTextField tfAddGroup;
	public JTextField tfAddType;
	public JButton btnAddGroup;
	public JButton btnAddType;

	/**
	 * Create the frame.
	 */
	public Song() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
		setBounds(100, 100, 780, 371);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		tfName = new JTextField();
		tfName.setFont(new Font("Sitka Display", Font.BOLD, 16));
		tfName.setBounds(164, 44, 242, 42);
		contentPane.add(tfName);
		tfName.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Song's Name:");
		lblNewLabel.setFont(new Font("Sitka Display", Font.BOLD, 20));
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(10, 42, 145, 44);
		contentPane.add(lblNewLabel);
		
		comboBoxGroup = new JComboBox();
		comboBoxGroup.setFont(new Font("Sitka Display", Font.BOLD, 17));
		comboBoxGroup.setBounds(164, 96, 242, 44);
		contentPane.add(comboBoxGroup);
		
		JLabel lblSongsGroup = new JLabel("Song's Group:");
		lblSongsGroup.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSongsGroup.setFont(new Font("Sitka Display", Font.BOLD, 20));
		lblSongsGroup.setBounds(10, 96, 145, 44);
		contentPane.add(lblSongsGroup);
		
		comboBoxGen = new JComboBox<Gen>();
		comboBoxGen.setFont(new Font("Sitka Display", Font.BOLD, 17));
		comboBoxGen.setBounds(164, 150, 242, 44);
		contentPane.add(comboBoxGen);
		
		JLabel lblSongsType = new JLabel("Song's Type:");
		lblSongsType.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSongsType.setFont(new Font("Sitka Display", Font.BOLD, 20));
		lblSongsType.setBounds(10, 150, 145, 44);
		contentPane.add(lblSongsType);
		
		lblDuration = new JLabel("Duration:");
		lblDuration.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDuration.setFont(new Font("Sitka Display", Font.BOLD, 20));
		lblDuration.setBounds(10, 204, 145, 44);
		contentPane.add(lblDuration);
		
		tfDuration = new JTextField();
		tfDuration.setFont(new Font("Sitka Display", Font.BOLD, 17));
		tfDuration.setBounds(164, 204, 69, 42);
		contentPane.add(tfDuration);
		tfDuration.setColumns(10);
		
		btnInsert = new JButton("Insert");
		btnInsert.setFont(new Font("Sitka Display", Font.BOLD, 15));
		btnInsert.setBounds(125, 270, 108, 54);
		contentPane.add(btnInsert);
		
		btnDelete = new JButton("Delete");
		btnDelete.setFont(new Font("Sitka Display", Font.BOLD, 15));
		btnDelete.setBounds(243, 270, 114, 54);
		contentPane.add(btnDelete);
		
		tfAddGroup = new JTextField();
		tfAddGroup.setFont(new Font("Sitka Display", Font.BOLD, 16));
		tfAddGroup.setColumns(10);
		tfAddGroup.setBounds(416, 98, 242, 42);
		contentPane.add(tfAddGroup);
		
		btnAddGroup = new JButton("ADD");
		btnAddGroup.setFont(new Font("Sitka Display", Font.BOLD, 18));
		btnAddGroup.setBounds(668, 96, 88, 42);
		contentPane.add(btnAddGroup);
		
		tfAddType = new JTextField();
		tfAddType.setFont(new Font("Sitka Display", Font.BOLD, 16));
		tfAddType.setColumns(10);
		tfAddType.setBounds(416, 150, 242, 42);
		contentPane.add(tfAddType);
		
		btnAddType = new JButton("ADD");
		btnAddType.setFont(new Font("Sitka Display", Font.BOLD, 18));
		btnAddType.setBounds(668, 150, 88, 42);
		contentPane.add(btnAddType);
	}
}
