package View;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JPasswordField;

public class Login extends JFrame {

	public JPanel contentPane;
	public JTextField tfUsername;
	public JPasswordField passField;
	public JButton btnLogin;
	public JButton btnLogUp;

	/**
	 * Create the frame.
	 */
	public Login() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		tfUsername = new JTextField();
		tfUsername.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		tfUsername.setBounds(145, 67, 170, 29);
		contentPane.add(tfUsername);
		tfUsername.setColumns(10);
		
		JLabel lblLogin = new JLabel("Username:");
		lblLogin.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLogin.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblLogin.setBounds(50, 67, 85, 34);
		contentPane.add(lblLogin);
		
		JLabel lblNewLabel = new JLabel("Password:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lblNewLabel.setBounds(59, 114, 76, 26);
		contentPane.add(lblNewLabel);
		
		btnLogin = new JButton("Sign In");
		btnLogin.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnLogin.setBounds(145, 170, 85, 29);
		contentPane.add(btnLogin);
		
		btnLogUp = new JButton("Sign Up");
		btnLogUp.setFont(new Font("Times New Roman", Font.BOLD, 14));
		btnLogUp.setBounds(226, 170, 90, 29);
		contentPane.add(btnLogUp);
		
		passField = new JPasswordField();
		passField.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		passField.setBounds(145, 111, 170, 29);
		contentPane.add(passField);
	}
}
