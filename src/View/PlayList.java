package View;



import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Model.User_Playlist;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JSeparator;
import javax.swing.JComboBox;

public class PlayList extends JFrame {

	private JPanel contentPane;
	public JTextField tfNewPlayList;
	public JButton btnCreatePlaylist;
	public JComboBox<User_Playlist> comboBoxPlayList;
	public JButton btnAddSong;


	/**
	 * Create the frame.
	 */
	public PlayList() {
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 251);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("New Playlist");
		lblNewLabel.setFont(new Font("Sitka Display", Font.BOLD, 15));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(93, 10, 103, 27);
		contentPane.add(lblNewLabel);
		
		tfNewPlayList = new JTextField();
		tfNewPlayList.setFont(new Font("Sitka Display", Font.BOLD, 15));
		tfNewPlayList.setBounds(49, 47, 192, 27);
		contentPane.add(tfNewPlayList);
		tfNewPlayList.setColumns(10);
		
		btnCreatePlaylist = new JButton("Create Playlist");
		btnCreatePlaylist.setFont(new Font("Sitka Display", Font.BOLD, 15));
		btnCreatePlaylist.setBounds(256, 47, 131, 27);
		contentPane.add(btnCreatePlaylist);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 105, 416, 2);
		contentPane.add(separator);
		
		JLabel lblNewLabel_1 = new JLabel("Choose Playlist");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Sitka Display", Font.BOLD, 15));
		lblNewLabel_1.setBounds(85, 116, 131, 21);
		contentPane.add(lblNewLabel_1);
		
		comboBoxPlayList = new JComboBox<>();
		comboBoxPlayList.setBounds(49, 147, 192, 27);
		contentPane.add(comboBoxPlayList);
		
		btnAddSong = new JButton("Add Song");
		btnAddSong.setFont(new Font("Segoe UI Semibold", Font.BOLD, 15));
		btnAddSong.setBounds(256, 147, 131, 27);
		contentPane.add(btnAddSong);
	}
}
